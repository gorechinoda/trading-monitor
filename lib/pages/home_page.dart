import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trading_interview/my_widgets/averages.dart';
import 'package:trading_interview/my_widgets/candles_chat.dart';
import 'package:trading_interview/my_widgets/loading_text.dart';
import 'package:trading_interview/providers/instances_provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Forex Trading"),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(48.0),
          child: Container(
            height: 48.0,
            alignment: Alignment.center,
            child: context.watch<InstancesProvider>().progress < 100
                ? const LoadingText()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Averages("USD",
                          context.watch<InstancesProvider>().usdAverages),
                      Averages("EUR",
                          context.watch<InstancesProvider>().eurAverages),
                    ],
                  ),
          ),
        ),
      ),
      body: SafeArea(
        minimum: const EdgeInsets.all(24.0),
        child: context.watch<InstancesProvider>().progress < 100
            ? const LoadingText()
            : const CandlesChat(),
      ),
    );
  }
}
