import 'package:flutter/material.dart';
import 'package:trading_interview/models/currency_averages.dart';

class Averages extends StatelessWidget {
  final String heading;
  final CurrencyAverages currencyAverage;

  const Averages(this.heading, this.currencyAverage, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: SizedBox(
        width: 100,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "$heading averages",
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: const [
                    Text(
                      "Low  :",
                    ),
                    Text(
                      "High :",
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(currencyAverage.lowAverage.toStringAsFixed(2)),
                    Text(currencyAverage.lowAverage.toStringAsFixed(2)),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
