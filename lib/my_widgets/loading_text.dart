import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trading_interview/providers/instances_provider.dart';

class LoadingText extends StatelessWidget {
  const LoadingText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "${context.watch<InstancesProvider>().progress}% Loading ...",
      ),
    );
  }
}
