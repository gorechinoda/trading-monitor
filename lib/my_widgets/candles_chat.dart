import 'package:flutter/material.dart';
import 'package:interactive_chart/interactive_chart.dart';
import 'package:provider/provider.dart';
import 'package:trading_interview/providers/instances_provider.dart';

class CandlesChat extends StatelessWidget {
  const CandlesChat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InteractiveChart(
      initialVisibleCandleCount: 100,
      candles: context.watch<InstancesProvider>().candles,
      style: ChartStyle(
        priceGainColor: Colors.teal[200]!,
        priceLossColor: Colors.blueGrey,
        volumeColor: Colors.teal.withOpacity(0.8),
        trendLineStyles: [
          Paint()
            ..strokeWidth = 2.0
            ..strokeCap = StrokeCap.round
            ..color = Colors.deepOrange,
          Paint()
            ..strokeWidth = 4.0
            ..strokeCap = StrokeCap.round
            ..color = Colors.orange,
        ],
        priceGridLineColor: Colors.blue[200]!,
        priceLabelStyle: TextStyle(color: Colors.blue[200]),
        timeLabelStyle: TextStyle(color: Colors.blue[200]),
        selectionHighlightColor: Colors.red.withOpacity(0.2),
        overlayBackgroundColor: Colors.red[900]!.withOpacity(0.6),
        overlayTextStyle: TextStyle(color: Colors.red[100]),
        timeLabelHeight: 32,
        volumeHeightFactor: 0.2,
      ),
    );
  }
}
