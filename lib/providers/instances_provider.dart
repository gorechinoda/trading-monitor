import 'dart:async';

import 'package:flutter/material.dart';
import 'package:interactive_chart/interactive_chart.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:trading_interview/models/currency_averages.dart';

class InstancesProvider with ChangeNotifier {
  final List<CandleData> _data = [];
  int _progress = 0;
  final _usdAverages = CurrencyAverages();
  final _eurAverages = CurrencyAverages();

  List<CandleData> get candles => _data;
  int get progress => _progress;
  CurrencyAverages get usdAverages => _usdAverages;
  CurrencyAverages get eurAverages => _eurAverages;

  InstancesProvider() {
    Timer.periodic(
      const Duration(seconds: 5),
      (timer) async {
        await reloadCandles();
      },
    );
  }

  Future<void> reloadCandles() async {
    var url = Uri.https(
      'apiv2.bitcoinaverage.com',
      '/indices/global/ticker/all',
      {"crypto": "BTC", "fiat": "USD,EUR"},
    );
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url,
        headers: {"x-ba-key": "NDZjOGMxOTY0NDlkNGNjOTk1MjBmZTFmYWYxM2M5NDk"});
    if (response.statusCode == 200) {
      var jsonResponse =
          convert.jsonDecode(response.body) as Map<String, dynamic>;
      addCandlesToList(jsonResponse);
      if (_progress < 100) {
        _progress += 2;
      }
      notifyListeners();
    }
  }

  void addCandlesToList(Map<String, dynamic> data) {
    var btcusd = data["BTCUSD"] as Map<String, dynamic>;
    var btceur = data["BTCEUR"] as Map<String, dynamic>;
    _data.add(
      CandleData(
        timestamp: btcusd["timestamp"],
        open: btcusd["open"]["hour"],
        close: btcusd["last"],
        volume: btcusd["volume"],
        high: btcusd["high"],
        low: btcusd["low"],
      ),
    );

    _data.add(
      CandleData(
        timestamp: btceur["timestamp"],
        open: btceur["open"]["hour"],
        close: btceur["last"],
        volume: btceur["volume"],
        high: btceur["high"],
        low: btceur["low"],
      ),
    );

    if (_data.length > 100) {
      _data.removeAt(0);
      _data.removeAt(0);
    }
    calculateAverages();
  }

  void calculateAverages() {
    for (var i = 0; i < _data.length; i++) {
      var candle = _data[i];
      if (i % 2 == 0) {
        usdAverages.highAverage += candle.high ?? 0;
        usdAverages.lowAverage += candle.low ?? 0;
      } else {
        eurAverages.highAverage += candle.high ?? 0;
        eurAverages.lowAverage += candle.low ?? 0;
      }
    }
    var numberOfPares = _data.length / 2;
    usdAverages.highAverage = usdAverages.highAverage / numberOfPares;
    usdAverages.lowAverage = usdAverages.lowAverage / numberOfPares;

    eurAverages.highAverage = eurAverages.highAverage / numberOfPares;
    eurAverages.lowAverage = eurAverages.lowAverage / numberOfPares;
  }
}
